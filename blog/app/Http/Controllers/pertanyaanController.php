<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pertanyaanController extends Controller
{
    public function create(){
        return view('question.create');
    }

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        // dd($request->title);
        $query = DB::table('question')->insert([
            "title" => $request['title'],
            "body" => $request['body']
        ]);
        return redirect('/pertanyaan')->with('success', 'Success, kindly wait others to reply');
    }

    public function homequestion(){
        $questions = DB::table('question')->get();
        // dd($questions[0]);
        return view('question.home', compact('questions'));
    }

    public function show ($pertanyaan_id){
        $question = DB::table('question')->where('id', $pertanyaan_id)->first();
        return view('question.show', compact('question'));
    }

    public function edit ($pertanyaan_id){
        $question = DB::table('question')->where('id', $pertanyaan_id)->first();
        return view('question.edit', compact('question'));
    }

    public function update (Request $request, $pertanyaan_id){
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        $affected = DB::table('question')
                        ->where('id', $pertanyaan_id)
                        ->update([
                            'title' => $request['title'],
                            'body' => $request['body']
                        ]);
        return redirect('/pertanyaan')->with('success', 'Update success');
    }

    public function destroy ($pertanyaan_id){
        $query = DB::table('question')
                            ->where('id', $pertanyaan_id)
                            ->delete();
        return redirect('/pertanyaan')->with('success', 'Delete success'); 
    }
}
